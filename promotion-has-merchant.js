const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')

class PromotionHasMerchant extends Model {}

PromotionHasMerchant.init({
  id: {
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  promotionId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    references: {
      model: 'promotions',
      key: 'id'
    },
    field: 'promotion_id'
  },
  merchantId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    references: {
      model: 'merchants',
      key: 'id'
    },
    field: 'merchant_id'
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: DataTypes.DATE
  },
  deletedAt: {
    field: 'deleted_at',
    type: DataTypes.DATE
  },
}, {
  tableName: 'promotions_has_merchants',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'PromotionHasMerchant'
})

module.exports = {
  PromotionHasMerchant,
}

