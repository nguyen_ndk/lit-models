const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {PURCHASE_CONFIG} = require('lit-constants')


class PurchaseLine extends Model {}

PurchaseLine.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
  label: {
    type: DataTypes.STRING
  },
  type: {
    type: DataTypes.STRING,
    allowNull: false
  },
  dueDate: {
    field: 'due_date',
    type: DataTypes.DATE
  },
  amount: {
    type: DataTypes.FLOAT,
    allowNull: false
  },
  operator: {
    type: DataTypes.ENUM(PURCHASE_CONFIG.OPERATOR.DUE, PURCHASE_CONFIG.OPERATOR.PAYMENT)
  },
  note: {
    type: DataTypes.TEXT
  },
  userId: {
    field: 'user_id',
    type: DataTypes.INTEGER,
    allowNull: false
  },
  paymentGatewayRequestId: {
    field: 'payment_gateway_request_id',
    type: DataTypes.INTEGER,
    allowNull: true
  },
  purchaseId: {
    field: 'purchase_id',
    type: DataTypes.INTEGER,
    allowNull: true
  },
  lateFeeId: {
    field: 'late_fee_id',
    type: DataTypes.INTEGER
  },
  purchaseInstallmentId: {
    field: 'purchase_installment_id',
    type: DataTypes.INTEGER
  },
  promotionId: {
    field: 'promotion_id',
    type: DataTypes.INTEGER
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE,
    allowNull: false
  },
  updatedAt: {
    field: 'updated_at',
    type: DataTypes.DATE
  },
  deletedAt: {
    field: 'deleted_at',
    type: DataTypes.DATE
  },
}, {
  tableName: 'purchase_lines',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'PurchaseLine'
})

module.exports = {
  PurchaseLine,
}

