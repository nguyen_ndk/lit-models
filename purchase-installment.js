const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {LateFee} = require('./late-fee')
const {PurchaseLine} = require('./purchase-line')

class PurchaseInstallment extends Model {}

PurchaseInstallment.init({
  purchaseId: {
    type: DataTypes.INTEGER,
    field: 'purchase_id'
  },
  userId: {
    type: DataTypes.INTEGER,
    field: 'user_id'
  },
  installmentNumber: {
    type: DataTypes.TINYINT,
    field: 'number'
  },
  amount: {
    type: DataTypes.INTEGER
  },
  status: {
    type: DataTypes.STRING(100)
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: 'updated_at'
  },
  createdAt: {
    type: DataTypes.DATE,
    field: 'created_at'
  },
  paymentMethodId: {
    type: DataTypes.INTEGER,
    field: 'payment_method_id'
  },
  dueDate: {
    field: 'due_date',
    type:DataTypes.DATE
  },
  paidAt: {
    type:DataTypes.DATE,
    field: 'paid_at'
  },
  failedReason: {
    type: DataTypes.STRING,
    field: 'failed_reason'
  },
  transactionId: {
    type: DataTypes.STRING(36),
    field: 'tran_id'
  },
  refundedAt: {
    type:DataTypes.DATE,
    field: 'refunded_at'
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false
  },
}, {
  tableName: 'purchases_installment',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'PurchaseInstallment'
})

PurchaseInstallment.associate = (models) => {
  PurchaseInstallment.belongsTo(models.payment_methods, {foreignKey: 'paymentMethodId'})
  PurchaseInstallment.belongsTo(models.purchases, {foreignKey: 'purchaseId'})
}

LateFee.belongsTo(PurchaseInstallment, {foreignKey: 'purchaseInstallmentId'})
PurchaseInstallment.hasMany(LateFee, {foreignKey: 'purchaseInstallmentId'})

PurchaseLine.belongsTo(PurchaseInstallment, {foreignKey: 'purchaseInstallmentId'})
PurchaseInstallment.hasMany(PurchaseLine, {foreignKey: 'purchaseInstallmentId', as: 'PurchaseLineLateFee'})



module.exports = {
  PurchaseInstallment
}
