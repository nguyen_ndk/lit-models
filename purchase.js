const { DataTypes, Model } = require('sequelize')
const _ = require('lodash')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {PURCHASE} = require('lit-constants')
const {PurchaseInstallment} = require('./purchase-installment')
const {PaymentMethod} = require('./payment-method')
const {PurchaseAccounting} = require('./purchase-accounting')
const {LateFee} = require('./late-fee')

class Purchase extends Model {}

Purchase.init({
  // Model attributes are defined here
  userId: {
    type: DataTypes.INTEGER,
    field: 'user_id'
  },
  merchantId: {
    type: DataTypes.STRING,
    allowNull: false,
    field: 'merchant_id'
  },
  amount: {
    type: DataTypes.INTEGER
  },

  originalAmount: {
    type: DataTypes.INTEGER,
    field: 'original_amount'
  },
  paymentStatus: {
    type: DataTypes.TINYINT,
    field: 'payment_status'
  },
  merchantPaidStatus: {
    type: DataTypes.TINYINT,
    field: 'merchant_paid_status'
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: 'updated_at'
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    field: 'created_at'
  },
  deliveryDate: {
    type: DataTypes.DATEONLY,
    field: 'delivery_date'
  },
  deliveryStatus: {
    type: DataTypes.TINYINT,
    field: 'delivery_status'
  },
  merchantRequestId: {
    type: DataTypes.STRING,
    field: 'merchant_request_id'
  },
  merchantOrderId : {
    type: DataTypes.STRING,
    field: 'merchant_order_id'
  },
  returnUrl: {
    type: DataTypes.STRING,
    field: 'return_url'
  },
  notifyUrl: {
    type: DataTypes.STRING,
    field: 'notify_url'
  },
  version: {
    type: DataTypes.BOOLEAN
  },
  voucherId: {
    type: DataTypes.INTEGER,
    field: 'voucher_id'
  },
  totalInstallments: {
    type: DataTypes.TINYINT,
    field: 'total_installments'
  },
  paidInstallments: {
    type: DataTypes.TINYINT,
    field: 'paid_installments'
  },
  dueInstallments: {
    type: DataTypes.TINYINT,
    field: 'due_installments'
  },
  transactionId: {
    type: DataTypes.STRING,
    field: 'tran_id'
  },
  errorCode: {
    type: DataTypes.INTEGER,
    field: 'error_code'
  },
  errorMessage: {
    type: DataTypes.STRING,
    field: 'error_msg'
  },
  source: {
    type: DataTypes.ENUM(_.values(PURCHASE.SOURCE)),
    allowNull: false
  },
  billId: {
    type: DataTypes.STRING,
    field: 'bill_id'
  },
  payFirstInstallmentAt: {
    type: DataTypes.DATE,
    field: 'pay_first_installment_at'
  },
  payLastInstallmentAt: {
    type: DataTypes.DATE,
    field: 'pay_last_installment_at'
  },
  payeeAgent: {
    type: DataTypes.ENUM(_.values(PURCHASE.PAYEE_AGENT)),
    field: 'payee_agent'
  },
  description: {
    type: DataTypes.STRING
  },
  dueGap: {
    type: DataTypes.INTEGER,
    field: 'due_gap'
  },
  paymentMethodId: {
    type: DataTypes.INTEGER,
    field: 'payment_method_id'
  },

  promotionId: {
    type: DataTypes.INTEGER,
    field: 'promotion_id'
  },

  amountDiscount: {
    type: DataTypes.INTEGER,
    field: 'discount_amount'
  },
  merchantConfirmDeliveryAt: {
    type: DataTypes.DATE,
    field: 'delivered_at'
  },
  refundedAt: {
    type: DataTypes.DATE,
    field: 'refunded_at'
  },
  adminFeeAmount: {
    type: DataTypes.FLOAT,
    field: 'admin_fee_amount'
  },
  accountingSyncStatus: {
    type: DataTypes.STRING(50),
    allowNull: false,
    defaultValue: PURCHASE.ACCOUNTING_SYNC_STATUS.NEW,
    field: 'accounting_sync_status'
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false
  },
}, {
  tableName: 'purchases',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'Purchase'
})
Purchase.hasMany(PurchaseInstallment, {foreignKey: 'purchaseId'})
PurchaseInstallment.belongsTo(Purchase, {foreignKey: 'purchaseId'})

PaymentMethod.hasMany(Purchase, {foreignKey: 'paymentMethodId'})
Purchase.belongsTo(PaymentMethod, {foreignKey: 'paymentMethodId'})

PurchaseAccounting.belongsTo(Purchase, {foreignKey: 'purchaseId'})
Purchase.hasMany(PurchaseAccounting, {foreignKey: 'purchaseId'})

LateFee.belongsTo(Purchase, {foreignKey: 'purchaseId'})
Purchase.hasMany(LateFee, {foreignKey: 'purchaseId'})

module.exports = {
  Purchase,
}
