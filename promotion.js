const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {Files} = require('./files')
const { UserHasPromotion } = require('./user-has-promotion')
class Promotion extends Model {}

Promotion.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
  name: {
    type: DataTypes.STRING(250),
    allowNull: false
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: true
  },
  code: {
    type: DataTypes.STRING(20),
    allowNull: false
  },
  isFeatured: {
    type: DataTypes.TINYINT,
    allowNull: false,
    defaultValue: 0,
    field: 'is_featured'
  },
  datetimeStart: {
    type: DataTypes.DATE,
    allowNull: false,
    field: 'datetime_start'
  },
  datetimeEnd: {
    type: DataTypes.DATE,
    allowNull: true,
    field: 'datetime_end'
  },
  duration: {
    type: DataTypes.INTEGER,
    allowNull: true
  },
  amount: {
    type: DataTypes.FLOAT,
    allowNull: false
  },
  amountType: {
    type: DataTypes.INTEGER,
    allowNull: false,
    field: 'amount_type'
  },
  maxFlatAmount: {
    type: DataTypes.FLOAT,
    allowNull: true,
    field: 'max_flat_amount'
  },
  minPurchaseAmount: {
    type: DataTypes.FLOAT,
    allowNull: true,
    field: 'min_purchase_amount'
  },
  type: {
    type: DataTypes.INTEGER,
    allowNull: true
  },
  appliedTo: {
    type: DataTypes.STRING(40),
    allowNull: true,
    field: 'applied_to'
  },
  maxQtyPerUser:{
    type: DataTypes.INTEGER,
    allowNull: true,
    field: 'max_qty_per_user'
  },
  isCompanyRestricted: {
    type: DataTypes.TINYINT,
    allowNull: false,
    defaultValue: 0,
    field: 'is_company_restricted'
  },
  isMerchantRestricted: {
    type: DataTypes.TINYINT,
    allowNull: false,
    defaultValue: 0,
    field: 'is_merchant_restricted'
  },
  isCategoryRestricted: {
    type: DataTypes.TINYINT,
    allowNull: false,
    defaultValue: 0,
    field: 'is_category_restricted'
  },
  isUserRestricted: {
    type: DataTypes.TINYINT,
    allowNull: false,
    defaultValue: 0,
    field: 'is_user_restricted'
  },
  isArchive:{
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false,
    field:'is_archive'
  },
  quantityMax: {
    type: DataTypes.INTEGER,
    allowNull: true,
    field: 'quantity_max'
  },
  quantityUsed: {
    type: DataTypes.INTEGER,
    allowNull: true,
    defaultValue: 0,
    field: 'quantity_used'
  },
  budgetMax: {
    type: DataTypes.FLOAT,
    allowNull: true,
    field: 'budget_max'
  },
  budgetUsed: {
    type: DataTypes.FLOAT,
    allowNull: true,
    defaultValue: 0,
    field: 'budget_used'
  },

  isCheckout: {
    type: DataTypes.BOOLEAN,
    allowNull: true,
    field: 'is_checkout'
  },
  rewardForm: {
    type: DataTypes.INTEGER,
    allowNull: true,
    field: 'rewards_from'
  },
  partnerName: {
    type: DataTypes.STRING(255),
    allowNull: true,
    field: 'parter_name'
  },
  maxVoucherCanClaim: {
    type: DataTypes.INTEGER,
    allowNull: true,
    field: 'max_voucher_can_claim'
  },
  triggerEventCode: {
    type: DataTypes.STRING(20),
    allowNull: true,
    field: 'trigger_event_code'
  },
  fileIconId: {
    type: DataTypes.INTEGER,
    references: {
      model: 'files',
      key: 'id'
    },
    field: 'file_icon_id'
  },
  fileThumbnailId: {
    type: DataTypes.INTEGER,
    references: {
      model: 'files',
      key: 'id'
    },
    field: 'file_thumbnail_id'
  },
  fileBannerId: {
    type: DataTypes.INTEGER,
    references: {
      model: 'files',
      key: 'id'
    },
    field: 'file_banner_id'
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    field: 'created_at'
  },
  updatedAt: {
    type: DataTypes.DATE,
    allowNull: true,
    field: 'updated_at'
  },
  deletedAt: {
    type: DataTypes.DATE,
    allowNull: true,
    field: 'deleted_at'
  }
}, {
  tableName: 'promotions',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'Promotion',
  paranoid: true,
})

Promotion.hasOne(Files, {as: 'FileIcon', sourceKey: 'fileIconId', foreignKey: 'id'})
Promotion.hasOne(Files, {as: 'FileThumbnail', sourceKey: 'fileThumbnailId', foreignKey: 'id'})
Promotion.hasOne(Files, {as: 'FileBanner', sourceKey: 'fileBannerId', foreignKey: 'id'})
Promotion.hasMany(UserHasPromotion, {sourceKey: 'id', foreignKey: 'promotionId'})
UserHasPromotion.belongsTo(Promotion,  {foreignKey: 'promotionId'})


module.exports = {
  Promotion,
}

