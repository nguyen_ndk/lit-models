const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')

class PromotionHasCompany extends Model {}

PromotionHasCompany.init({
  id: {
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  promotionId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    references: {
      model: 'promotions',
      key: 'id'
    },
    field: 'promotion_id'
  },
  companyId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    references: {
      model: 'company',
      key: 'id'
    },
    field: 'company_id'
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: DataTypes.DATE
  },
  deletedAt: {
    field: 'deleted_at',
    type: DataTypes.DATE
  },
}, {
  tableName: 'promotions_has_company',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'PromotionHasCompany'
})

module.exports = {
  PromotionHasCompany,
}

