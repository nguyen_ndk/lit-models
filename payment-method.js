const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
class PaymentMethod extends Model {}

PaymentMethod.init({
  // Model attributes are defined here
  id: {
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: 'users',
      key: 'id'
    },
    field: 'user_id'
  },
  bankToken: {
    type: DataTypes.STRING(255),
    allowNull: true,
    field: 'bank_token'
  },
  ccCropped: {
    type: DataTypes.STRING(50),
    allowNull: true,
    field: 'cc_cropped'
  },
  ccValidity: {
    type: DataTypes.DATEONLY,
    allowNull: true,
    field: 'cc_validity'
  },
  bankDetail: {
    type: DataTypes.STRING(255),
    allowNull: true,
    field: 'bank_detail'
  },
  cardDetail: {
    type: DataTypes.STRING(100),
    allowNull: true,
    field: 'card_detail'
  },
  status: {
    type: DataTypes.STRING(40),
    allowNull: true
  },
  cardBrand: { //TODO : rename cardBrand => bankName
    type: DataTypes.STRING(255),
    allowNull: true,
    field: 'bank_name'
  },
  gateway: {
    type: DataTypes.STRING(50),
    allowNull: true
  },
  type: {
    type: DataTypes.STRING(20),
    allowNull: true
  },
  updatedBy: {
    type: DataTypes.INTEGER,
    allowNull: true,
    field: 'updated_by'
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: true,
    field: 'created_at'
  },
  updatedAt: {
    type: DataTypes.DATE,
    allowNull: true,
    field: 'updated_at'
  },
  deletedAt: {
    type: DataTypes.DATE,
    allowNull: true,
    field: 'deleted_at'
  }
}, {
  tableName: 'payment_methods',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'PaymentMethod'
})
PaymentMethod.associate = (models) => {
  PaymentMethod.belongsTo(models.users, {foreignKey: 'userId'})
}

module.exports = {
  PaymentMethod,
}

