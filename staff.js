const { DataTypes, Model } = require('sequelize')
const {
  facade: { DBFacade },
  providers: { DB_PROVIDER },
} = require('lit-utils')
const {MerchantsHasStaffs} = require('./merchants-has-staffs')
const {Merchant} = require('./merchant')
class Staff extends Model {}

Staff.init(
  {
    // Model attributes are defined here
    password: {
      type: DataTypes.STRING,
    },
    email: {
      type: DataTypes.STRING,
    },
    role: {
      type: DataTypes.CHAR,
    },
    position: {
      type: DataTypes.STRING,
    },
    name: {
      type: DataTypes.STRING,
    },
    status: {
      type: DataTypes.INTEGER,
    },
    companyId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'company_id',
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
    },
    deletedAt: {
      type: DataTypes.DATE,
      field: 'deleted_at'
    },
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      unique: true,
      allowNull: false,
    },
  },
  {
    tableName: 'staffs',
    sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
    paranoid: true,
    modelName: 'Staff',
  }
)


Staff.belongsToMany(Merchant, { through: MerchantsHasStaffs, foreignKey: 'StaffId'})
Merchant.belongsToMany(Staff, { through: MerchantsHasStaffs, foreignKey: 'MerchantId'})

module.exports = {
  Staff,
}
