const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {PosDevice} = require('./pos-device')
const {Purchase} = require('./purchase')

class MobilePosPurchase extends Model {}

MobilePosPurchase.init({
  posDeviceId: {
    type: DataTypes.INTEGER,
    field: 'pos_device_id',
    allowNull: false
  },
  purchaseId: {
    type: DataTypes.INTEGER,
    field: 'purchase_id'
  },
}, {
  tableName: 'mobile_pos_purchases',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'MobilePosPurchase',
  updatedAt: false
})

MobilePosPurchase.belongsTo(Purchase, {foreignKey: 'purchaseId'})
Purchase.hasMany(MobilePosPurchase, {foreignKey: 'purchaseId'})

MobilePosPurchase.belongsTo(PosDevice, {foreignKey: 'posDeviceId'})
PosDevice.hasMany(MobilePosPurchase, {foreignKey: 'posDeviceId'})

module.exports = {MobilePosPurchase}

