const {DataTypes, Model} = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const { Session } = require('./session')
const {Purchase} = require('./purchase')
const {UserCreditHistory} = require('./user-credit-history')
const {KycRequestCounting} = require('./kyc-request-counting')
const {PaymentMethod} = require('./payment-method')
const {PaymentGatewayRequest} = require('./payment-gateway-request')
const {MobilePosPurchase} = require('./mobile-pos-purchase')
const {UserCompletion} = require('./user-completion')
const { KycResult } = require('./kyc-result')
const { UserNotificationChannel } = require('./user-notification-channel')
const {USER} = require('lit-constants')

class User extends Model {
}

User.init({
  // Model attributes are defined here
  firstName: {
    type: DataTypes.STRING(USER.LIMITATION.FIRSTNAME_LENGTH),
    field: 'first_name'
  },
  lastName: {
    type: DataTypes.STRING(USER.LIMITATION.LASTNAME_LENGTH),
    field: 'last_name'
  },
  phone: {
    type: DataTypes.STRING
  },
  newPhone: {
    type: DataTypes.STRING,
    field: 'new_phone'
  },
  newEmail: {
    type: DataTypes.STRING,
    field: 'new_email'
  },
  birthday: {
    type: DataTypes.DATEONLY
  },
  status: {
    type: DataTypes.STRING(100)
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: 'updated_at'
  },
  createdAt: {
    type: DataTypes.DATE,
    field: 'created_at'
  },
  gender: {
    type: DataTypes.STRING(20)
  },
  password: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
  },
  address: {
    type: DataTypes.STRING(255)
  },
  idPhotoUrl: {
    type: DataTypes.STRING,
    field: 'id_photo_url'
  },
  userPhotoWithIdUrl: {
    type: DataTypes.STRING,
    field: 'user_photo_with_id_url'
  },
  tier: {
    type: DataTypes.STRING(100)
  },
  credit: {
    type: DataTypes.INTEGER
  },
  financeId: {
    type: DataTypes.STRING(50),
    field: 'finance_id'
  },
  creditCap: {
    type: DataTypes.INTEGER,
    field: 'credit_cap'
  },
  avatarUrl: {
    type: DataTypes.STRING(255),
    field: 'avatar_url'
  },
  isProcessingKYC:{
    type: DataTypes.BOOLEAN,
    field: 'is_process_kyc'
  },
  lang: {
    type: DataTypes.STRING(2),
  },
  gen: {
    type: DataTypes.STRING(100),
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false
  },
}, {
  tableName: 'users',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'User'
})
User.hasMany(PaymentMethod, {foreignKey: 'userId'})
PaymentMethod.belongsTo(User, {foreignKey: 'userId'})
User.hasMany(Purchase, {foreignKey: 'userId'})
Purchase.belongsTo(User, {foreignKey: 'userId'})
User.hasMany(UserCreditHistory, {foreignKey: 'userId'})
UserCreditHistory.belongsTo(User, {foreignKey: 'userId'})
User.hasMany(KycRequestCounting, {foreignKey: 'userId'})
KycRequestCounting.belongsTo(User, {foreignKey: 'userId'})
User.hasMany(PaymentGatewayRequest, {foreignKey: 'userId'})
PaymentGatewayRequest.belongsTo(User, {foreignKey: 'userId'})
User.hasMany(MobilePosPurchase, {foreignKey: 'userId'})
MobilePosPurchase.belongsTo(User, {foreignKey: 'userId'})

User.hasOne(UserCompletion, {foreignKey: 'userId'})
UserCompletion.belongsTo(User, {foreignKey: 'userId'})

User.hasOne(KycResult, {foreignKey: 'userId'})
KycResult.belongsTo(User, {foreignKey: 'userId'})

User.hasMany(UserNotificationChannel, {foreignKey: 'userId'})
UserNotificationChannel.belongsTo(User, {foreignKey: 'userId'})

User.hasOne(Session, {foreignKey: 'userId'})
Session.belongsTo(User, {foreignKey: 'userId'})
module.exports = {
  User,
}

