const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')

class MerchantContract extends Model {}

MerchantContract.init({
  code: {
    type: DataTypes.STRING,
    allowNull: false
  },
  datetimeStart: {
    field: 'datetime_start',
    type: DataTypes.DATE
  },
  datetimeEnd: {
    field: 'datetime_end',
    type: DataTypes.DATE
  },
  filePath: {
    field: 'file_path',
    type: DataTypes.STRING,
    allowNull: false
  },
  reminder: {
    type: DataTypes.BOOLEAN
  },
  merchantId: {
    field: 'merchant_id',
    type: DataTypes.INTEGER,
    allowNull: false
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: DataTypes.DATE
  },
  deletedAt: {
    field: 'updated_at',
    type: DataTypes.DATE
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
}, {
  tableName: 'merchants_contracts',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'MerchantContract'
})

module.exports = {
  MerchantContract,
}

