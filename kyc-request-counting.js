const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')

class KycRequestCounting extends Model {}

KycRequestCounting.init({
  // Model attributes are defined here
  userId: {
    type: DataTypes.INTEGER,
    field: 'user_id'
  },
  provider: {
    type: DataTypes.STRING
  },
  api: {
    type: DataTypes.STRING
  },
  createdAt: {
    type: DataTypes.DATE
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
}, {
  tableName: 'kyc_request_counting',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'KycRequestCounting',
  updatedAt: false,
})

module.exports = {
  KycRequestCounting,
}

