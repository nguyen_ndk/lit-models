const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')

class MerchantContact extends Model {}

MerchantContact.init({
  title: {
    type: DataTypes.STRING
  },
  name: {
    type: DataTypes.STRING
  },
  position: {
    type: DataTypes.STRING
  },
  mobile1: {
    type: DataTypes.STRING,
    field: 'mobile_1'
  },
  mobile2: {
    type: DataTypes.STRING,
    field: 'mobile_2'
  },
  email1: {
    type: DataTypes.STRING,
    field: 'email_1'
  },
  email2: {
    type: DataTypes.STRING,
    field: 'email_2'

  },
  merchantId: {
    type: DataTypes.INTEGER,
    field: 'merchant_id',
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: DataTypes.DATE
  },
  deletedAt: {
    field: 'deleted_at',
    type: DataTypes.DATE
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
}, {
  tableName: 'merchant_contacts',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'MerchantContact',
  paranoid: true
})

module.exports = {
  MerchantContact,
}

