const {DataTypes, Model} = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const { MarketingHasCategory } = require('./marketing-has-category')
const { MARKETING } = require('lit-constants')
class Marketing extends Model {
}

Marketing.init({
  merchantId: {
    field: 'merchant_id',
    type: DataTypes.INTEGER,
    allowNull: false
  },
  banner: {
    type: DataTypes.STRING
  },
  utm: {
    type: DataTypes.STRING
  },
  merchantType: {
    type: DataTypes.STRING,
    field: 'merchant_type',
    defaultValue: MARKETING.MERCHANT_TYPE.ONLINE
  },
  webLink: {
    field: 'web_link',
    type: DataTypes.STRING
  },
  appstoreLink: {
    field: 'appstore_link',
    type: DataTypes.STRING
  },
  playstoreLink: {
    field: 'playstore_link',
    type: DataTypes.STRING
  },
  appgalleryLink: {
    field: 'appgallery_link',
    type: DataTypes.STRING
  },
  impressions: {
    type: DataTypes.INTEGER
  },
  clicks: {
    type: DataTypes.INTEGER
  },
  status: {
    type: DataTypes.BOOLEAN,
    defaultValue: MARKETING.STATUS.ENABLED
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: DataTypes.DATE
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
}, {
  tableName: 'marketing',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'Marketing'
})

Marketing.hasMany(MarketingHasCategory, {foreignKey:'marketingId'})

module.exports = {
  Marketing,
}

