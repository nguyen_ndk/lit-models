const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {CREDIT_HISTORY_TYPE} = require('lit-constants')
class UserCreditHistory extends Model {}
UserCreditHistory.init({
  // Model attributes are defined here
  userId: {
    type: DataTypes.INTEGER
  },
  amount: {
    type: DataTypes.INTEGER
  },
  purchaseId: {
    type: DataTypes.INTEGER
  },
  type: {
    type: DataTypes.ENUM(CREDIT_HISTORY_TYPE.CREDIT, CREDIT_HISTORY_TYPE.INSTALL_DEDUCT)
  },
  createdAt: {
    type: DataTypes.DATE
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false
  },
}, {
  tableName: 'users_credits_history',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'UserCreditHistory',
  updatedAt: false,
})
UserCreditHistory.associate = (models) => {
  UserCreditHistory.belongsTo(models.users, {foreignKey: 'userId'})
}
module.exports = {
  UserCreditHistory,
}

