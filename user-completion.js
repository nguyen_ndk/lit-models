const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
class UserCompletion extends Model {}

UserCompletion.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    field: 'user_id'
  },
  emailVerified: {
    type: DataTypes.BOOLEAN,
    field: 'email_verified',
    defaultValue: false
  },
  phoneVerified: {
    type: DataTypes.BOOLEAN,
    field: 'phone_verified',
    defaultValue: false
  },
  kycPassed: {
    type: DataTypes.BOOLEAN,
    field: 'kyc_passed',
    defaultValue: false
  },
  paymentMethodAdded: {
    type: DataTypes.BOOLEAN,
    field: 'payment_method_added',
    defaultValue: false
  },
  completion: {
    type: DataTypes.INTEGER(3),
    defaultValue: 0
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: 'updated_at'
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    field: 'created_at'
  },
}, {
  tableName: 'user_completions',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'UserCompletion',
})
module.exports = {UserCompletion}

