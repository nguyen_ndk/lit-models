const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')

class Files extends Model {}

Files.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
  diskName: {
    type: DataTypes.STRING(255),
    allowNull: false,
    field: 'disk_name'
  },
  fileName: {
    type: DataTypes.STRING(40),
    allowNull: false,
    field: 'file_name'
  },
  contentType: {
    type: DataTypes.STRING(40),
    allowNull: false,
    field: 'content_type'
  },
  fileSize: {
    type: DataTypes.INTEGER,
    allowNull: false,
    field: 'file_size'
  },
  title: {
    type: DataTypes.STRING(255),
    allowNull: true
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: true
  },
  field: {
    type: DataTypes.STRING(255),
    allowNull: true,
    field: 'field'
  },
  attachmentId: {
    type: DataTypes.INTEGER,
    allowNull: true,
    field: 'attachment_id'
  },
  attachmentType: {
    type: DataTypes.STRING(255),
    allowNull: true,
    field: 'attachment_type'
  },
  sortOrder: {
    type: DataTypes.INTEGER,
    allowNull: true,
    field: 'sort_order'
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    field: 'created_at'
  },
  updatedAt: {
    type: DataTypes.DATE,
    allowNull: true,
    field: 'updated_at'
  },
  deletedAt: {
    type: DataTypes.DATE,
    allowNull: true,
    field: 'deleted_at'
  }
}, {
  tableName: 'files',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'Files'
})

module.exports = {
  Files,
}

