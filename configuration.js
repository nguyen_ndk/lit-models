const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {CONFIGURATION} = require('lit-constants')
class Configuration extends Model {}

Configuration.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false
  },
  // Model attributes are defined here
  name: {
    type: DataTypes.STRING(CONFIGURATION.LIMITATION.MAX_NAME),
    allowNull: false
  },
  value: {
    type: DataTypes.STRING(CONFIGURATION.LIMITATION.MAX_VALUE),
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: 'updated_at'
  },
  createdAt: {
    type: DataTypes.DATE,
    field: 'created_at',
    allowNull: false
  },
}, {
  tableName: 'configuration',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'Configuration',
})
module.exports = {Configuration}

