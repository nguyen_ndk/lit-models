const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')

class Translation extends Model {}

Translation.init({
  // Model attributes are defined here
  en: {
    type: DataTypes.CHAR
  },
  vn: {
    type: DataTypes.CHAR
  },
  tag: {
    type: DataTypes.STRING
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false
  },
}, {
  tableName: 'translation',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'Translation',
  createdAt: false,
  updatedAt: false,
})

module.exports = {
  Translation,
}

