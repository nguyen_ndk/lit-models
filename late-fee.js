const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')

class LateFee extends Model {}

LateFee.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
  userId: {
    field: 'user_id',
    type: DataTypes.INTEGER
  },
  purchaseId: {
    field: 'purchase_id',
    type: DataTypes.INTEGER
  },
  purchaseInstallmentId: {
    type: DataTypes.INTEGER,
    field: 'purchase_installment_id'
  },
  smsLogId: {
    field: 'sms_log_id',
    type: DataTypes.INTEGER
  },
  amount: {
    type: DataTypes.FLOAT
  },
  percentage: {
    type: DataTypes.FLOAT
  },
  dueDate: {
    field: 'due_date',
    type: DataTypes.DATE
  },
  feeNumber: {
    field: 'fee_number',
    type: DataTypes.INTEGER
  },
  lateDaysFrequency: {
    field: 'late_days_frequency',
    type: DataTypes.INTEGER
  },
  constants: {
    type: DataTypes.STRING
  },
  cumulativeAmount: {
    field: 'cumulative_amount',
    type: DataTypes.FLOAT
  },
  cumulativePercentage: {
    field: 'cumulative_percentage',
    type: DataTypes.FLOAT
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: DataTypes.DATE
  },
}, {
  tableName: 'late_fees',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'LateFee'
})


module.exports = {
  LateFee,
}

