const { DataTypes, Model } = require('sequelize')
const _ = require('lodash')
const {facade: {DBFacade}, providers: {DB_PROVIDER, PAYMENT_GATEWAY_PROVIDER}} = require('lit-utils')
const {PGReqToPurchase} = require('./pg-req-to-purchase')
const {Purchase} = require('./purchase')
const {PaymentMethod} = require('./payment-method')

class PaymentGatewayRequest extends Model {}

PaymentGatewayRequest.init({
  // Model attributes are defined here
  type: { // TODO: remove it
    type: DataTypes.TINYINT
  },
  status: {
    type: DataTypes.STRING(100)
  },
  transactionId: {
    type: DataTypes.STRING,
    field: 'transaction_id'
  },
  gateway: {
    type: DataTypes.ENUM(_.values(PAYMENT_GATEWAY_PROVIDER))
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: 'updated_at'
  },
  userId: {
    type: DataTypes.INTEGER,
    field: 'user_id'
  },
  purchaseId: {
    type: DataTypes.INTEGER,
    field: 'purchase_id'
  },
  gatewayTranId: {
    type: DataTypes.STRING,
    field: 'gateway_tran_id'
  },
  amount: {
    type: DataTypes.INTEGER
  },
  installmentId: { // TODO: remove it
    type: DataTypes.INTEGER,
    field: 'installment_id'
  },
  paymentMethodId: {
    type: DataTypes.INTEGER,
    field: 'payment_method_id'
  },
  createdAt: {
    type: DataTypes.DATE,
    field: 'created_at'
  },
  retryCount: {
    type: DataTypes.INTEGER,
    field: 'retry_count'
  },
  installmentNumber: {
    type: DataTypes.TINYINT,
    field: 'installment_number'
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false
  },
}, {
  tableName: 'payment_gateway_requests',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'PaymentGatewayRequest'
})

Purchase.belongsToMany(PaymentGatewayRequest, {through: PGReqToPurchase, foreignKey: 'purchaseId'})
PaymentGatewayRequest.belongsToMany(Purchase, {through: PGReqToPurchase, foreignKey: 'PGReqId'})

PaymentMethod.hasMany(PaymentGatewayRequest, {foreignKey: 'paymentMethodId'})
PaymentGatewayRequest.belongsTo(PaymentMethod, {foreignKey: 'paymentMethodId'})

module.exports = {
  PaymentGatewayRequest,
}

