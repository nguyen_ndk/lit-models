const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {OnlineStore} = require('./online-store')
const {Address} = require('./address')
const {Purchase} = require('./purchase')
const {Company} = require('./company')
const {PosDevice} = require('./pos-device')
const {MobilePosPurchase} = require('./mobile-pos-purchase')
const {MerchantContact} = require('./merchant-contact')
const {Marketing} = require('./marketing')
const {Category} = require('./category')
const {MerchantCategory} = require('./merchant-category')
const {MerchantFinancial} = require('./merchant-financial')

class Merchant extends Model {}

Merchant.init({
  // Model attributes are defined here
  code: {
    type: DataTypes.STRING(10),
  },
  name: {
    type: DataTypes.STRING(50),
    allowNull: false
  },
  type: {
    type: DataTypes.STRING(100),
    allowNull: false
  },
  companyId: {
    type: DataTypes.INTEGER,
    field: 'company_id',
    allowNull: false
  },
  addressId: {
    type: DataTypes.INTEGER,
    field: 'address_id',
  },
  financeId: {
    type: DataTypes.STRING(50),
    field: 'finance_id',
  },
  thumbnail: {
    type: DataTypes.STRING(250)
  },
  clientId: {
    type: DataTypes.STRING(64),
    field: 'client_id',
    allowNull: false
  },
  status: {
    type: DataTypes.STRING(100)
  },
  adminFeeAmount: {
    type: DataTypes.FLOAT,
    field: 'admin_fee_amount',
  },
  adminFeeType: {
    type: DataTypes.STRING(100),
    field: 'admin_fee_type',
  },
  parentMarketingId: {
    type: DataTypes.INTEGER,
    field: 'parent_marketing_id',
    allowNull: true
  },
  createdAt: {
    type: DataTypes.DATE,
    field: 'created_at',
    allowNull: false
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: 'updated_at'
  },
  deletedAt: {
    type: DataTypes.DATE,
    field: 'deleted_at'
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false
  },
}, {
  tableName: 'merchants',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  paranoid: true,
  modelName: 'Merchant'
})

Merchant.hasOne(OnlineStore, {foreignKey: 'merchantId'})
OnlineStore.belongsTo(Merchant, {foreignKey: 'merchantId'})
Merchant.hasMany(Purchase, { foreignKey: 'merchantId' })
Purchase.belongsTo(Merchant, { foreignKey: 'merchantId' })
Merchant.belongsTo(Address, {foreignKey: 'addressId'})

Company.hasMany(Merchant, { foreignKey: 'company_id' })
Merchant.belongsTo(Company, { foreignKey: 'company_id' })

PosDevice.belongsTo(Merchant, {foreignKey: 'merchantId'})
Merchant.hasMany(PosDevice, {foreignKey: 'merchantId'})

MobilePosPurchase.belongsTo(Merchant, {foreignKey: 'merchantId'})
Merchant.hasMany(MobilePosPurchase, {foreignKey: 'merchantId'})
MerchantContact.belongsTo(Merchant, {foreignKey: 'merchantId'})
Merchant.hasMany(MerchantContact, {foreignKey: 'merchantId'})

Merchant.hasOne(Marketing, {foreignKey: 'merchantId'})
Marketing.belongsTo(Merchant, {foreignKey: 'merchantId'})

Category.belongsToMany(Merchant, { through: MerchantCategory, foreignKey: 'categoryId' })
Merchant.belongsToMany(Category, { through: MerchantCategory, foreignKey: 'merchantId' })

MerchantFinancial.belongsTo(Merchant, {foreignKey: 'merchantId' })
Merchant.hasMany(MerchantFinancial, {foreignKey: 'merchantId' })


Merchant.hasMany(MerchantCategory, { foreignKey: 'merchantId' })
MerchantCategory.belongsTo(Merchant, { foreignKey: 'merchant_id' })

module.exports = {Merchant}

