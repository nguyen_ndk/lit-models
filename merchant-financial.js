const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')

class MerchantFinancial extends Model {}

MerchantFinancial.init({
  paymentDays: {
    field: 'payment_days',
    type: DataTypes.INTEGER
  },
  paymentAmount: {
    field: 'payment_amount',
    type: DataTypes.FLOAT
  },
  paymentType: {
    field: 'payment_type',
    type: DataTypes.INTEGER
  },
  dateStart: {
    field: 'date_start',
    type: DataTypes.DATE
  },
  dateEnd: {
    field: 'date_end',
    type: DataTypes.DATE
  },
  financeId: {
    field: 'finance_id',
    type: DataTypes.STRING
  },
  merchantId: {
    field: 'merchant_id',
    type: DataTypes.INTEGER
  },
  status: {
    type: DataTypes.INTEGER
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: DataTypes.DATE
  },
  deletedAt: {
    field: 'deleted_at',
    type: DataTypes.DATE
  },
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
}, {
  tableName: 'merchant_financials',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  paranoid: true,
  modelName: 'MerchantFinancial'
})

module.exports = {
  MerchantFinancial,
}

