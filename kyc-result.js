const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
const {KalapaResponse} = require('./kalapa-response')

class KycResult extends Model {}


KycResult.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    unique: true,
    allowNull: false,
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  phraseOneStatus: {
    type: DataTypes.STRING(20),
    field: 'phrase_one_status'
  },
  phraseTwoStatus: {
    type: DataTypes.STRING(20),
    field: 'phrase_two_status'
  },
  score: {
    type: DataTypes.INTEGER(3),
    field: 'score'
  },
  idUrl: {
    type: DataTypes.STRING,
    field: 'id_url'
  },
  selfieUrl: {
    type: DataTypes.STRING,
    field: 'selfie_url'
  },
  status: {
    type: DataTypes.STRING(100),
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    field: 'created_at'
  },
}, {
  tableName: 'kyc_results',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'KycResult',
  updatedAt: false
})

KycResult.hasOne(KalapaResponse, {foreignKey: 'kycResultId'})
KalapaResponse.belongsTo(KycResult, {foreignKey: 'kycResultId'})

module.exports = {KycResult}

