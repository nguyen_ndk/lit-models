const { DataTypes, Model } = require('sequelize')
const {facade: {DBFacade}, providers: {DB_PROVIDER}} = require('lit-utils')
class PosNotificationChannel extends Model {}

PosNotificationChannel.init({
  id: {
    autoIncrement: true,
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true
  },
  identifier: {
    type: DataTypes.STRING,
  },
  channel: {
    type: DataTypes.STRING(50),
  },
  posId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    field: 'pos_id'
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE
  },
}, {
  tableName: 'pos_notification_channels',
  sequelize: DBFacade.provider(DB_PROVIDER.SEQUELIZE).getConnection(),
  modelName: 'PosNotificationChannel',
  updatedAt: false
})
module.exports = {PosNotificationChannel}

